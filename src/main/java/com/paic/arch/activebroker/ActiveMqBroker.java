package com.paic.arch.activebroker;

import static com.paic.arch.activebroker.SocketFinder.findNextAvailablePortBetween;
import static org.slf4j.LoggerFactory.getLogger;

import javax.jms.Connection;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.broker.Broker;
import org.apache.activemq.broker.BrokerService;
import org.apache.activemq.broker.region.DestinationStatistics;
import org.slf4j.Logger;

import com.paic.arch.JmsCallback;
import com.paic.arch.MessageBroker;
import com.paic.arch.NewMessageBroker;
import com.paic.arch.NoMessageReceivedException;

/**
 * ActiveMq
 * @author Administrator
 *
 */
public class ActiveMqBroker implements MessageBroker, NewMessageBroker {
    private static final Logger LOG = getLogger(ActiveMqBroker.class);
    private static final int ONE_SECOND = 1000;
    private static final int DEFAULT_RECEIVE_TIMEOUT = 10 * ONE_SECOND;
    public static final String DEFAULT_BROKER_URL_PREFIX = "tcp://localhost:";
    
    private String brokerUrl; // Mq地址
    private BrokerService brokerService; // ActiveMq服务
    private String message; // 等发送的消息
    
    public ActiveMqBroker() { }
    public ActiveMqBroker(String aBrokerUrl) {
    	brokerUrl = aBrokerUrl;
    }
    
	@Override
	public MessageBroker createARunningEmbeddedBrokerOnAvailablePort() throws Exception {
		return createARunningEmbeddedBrokerAt(DEFAULT_BROKER_URL_PREFIX + findNextAvailablePortBetween(41616, 50000));
	}

	@Override
	public String getBrokerUrl() {
		return brokerUrl;
	}

	@Override
	public void startEmbeddedBroker() throws Exception {
		brokerService.start();
	}

	@Override
	public void stopTheRunningBroker() throws Exception {
		if (brokerService == null) {
            throw new IllegalStateException("Cannot stop the broker from this API: " +
                    "perhaps it was started independently from this utility");
        }
        brokerService.stop();
        brokerService.waitUntilStopped();
	}

	@Override
	public NewMessageBroker bindToMqBrokerAt(String url) throws Exception {
		createARunningEmbeddedBrokerAt(url);
		return this;
	}
	
	@Override
	public MessageBroker bindToBrokerAtUrl(String url) {
		brokerUrl = url;
		return this;
	}

	@Override
	public MessageBroker sendATextMessageToDestinationAt(String inputQueue, String incomingTradeMessage) {
		executeCallbackAgainstRemoteBroker( inputQueue, (aSession, aDestination) -> {
            MessageProducer producer = aSession.createProducer(aDestination);
            producer.send(aSession.createTextMessage(incomingTradeMessage));
            producer.close();
            return "";
        });
        return this;
	}

	@Override
	public String retrieveASingleMessageFromTheDestination(String accountingQueue) {
		return retrieveASingleMessageFromTheDestination(accountingQueue, DEFAULT_RECEIVE_TIMEOUT);
	}

	@Override
	public String retrieveASingleMessageFromTheDestination(String aDestinationName, final int aTimeout) {
		return executeCallbackAgainstRemoteBroker(aDestinationName, (aSession, aDestination) -> {
            MessageConsumer consumer = aSession.createConsumer(aDestination);
            Message message = consumer.receive(aTimeout);
            if (message == null) {
                throw new NoMessageReceivedException(String.format("No messages received from the broker within the %d timeout", aTimeout));
            }
            consumer.close();
            return ((TextMessage) message).getText();
        });
	}

	@Override
	public MessageBroker andThen() {
		return this;
	}
	
	@Override
	public long getEnqueuedMessageCountAt(String aDestinationName) throws Exception {
        return getDestinationStatisticsFor(aDestinationName).getMessages().getCount();
    }

	@Override
    public boolean isEmptyQueueAt(String aDestinationName) throws Exception {
        return getEnqueuedMessageCountAt(aDestinationName) == 0;
    }

	@Override
	public NewMessageBroker then() {
		return this;
	}

	@Override
	public NewMessageBroker sendTheMessage(String inputMessage) {
		message = inputMessage;
        return this;
	}

	@Override
	public NewMessageBroker to(String inputQueue) {
		executeCallbackAgainstRemoteBroker(inputQueue, (aSession, aDestination) -> {
            MessageProducer producer = aSession.createProducer(aDestination);
            producer.send(aSession.createTextMessage(message));
            producer.close();
            return "";
        });
        return this;
	}

	@Override
	public String waitForAMessageOn(String accountingQueue) {
		return retrieveASingleMessageFromTheDestination(accountingQueue);
	}

	@Override
	public NewMessageBroker and() {
		return this;
	}

	private MessageBroker createARunningEmbeddedBrokerAt(String aBrokerUrl) throws Exception {
        LOG.debug("Creating a new broker at {}", aBrokerUrl);
        brokerUrl = aBrokerUrl;
        
        brokerService = new BrokerService();
        brokerService.setPersistent(false);
        brokerService.addConnector(aBrokerUrl);
        
        startEmbeddedBroker();
        
        return this;
    }
	
    private String executeCallbackAgainstRemoteBroker(String aDestinationName, JmsCallback aCallback) {
        Connection connection = null;
        String returnValue = "";
        try {
            ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(brokerUrl);
            connection = connectionFactory.createConnection();
            connection.start();
            returnValue = executeCallbackAgainstConnection(connection, aDestinationName, aCallback);
        } catch (JMSException jmse) {
            LOG.error("failed to create connection to {}", brokerUrl);
            throw new IllegalStateException(jmse);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (JMSException jmse) {
                    LOG.warn("Failed to close connection to broker at []", brokerUrl);
                    throw new IllegalStateException(jmse);
                }
            }
        }
        return returnValue;
    }

    private String executeCallbackAgainstConnection(Connection aConnection, String aDestinationName, JmsCallback aCallback) {
        Session session = null;
        try {
            session = aConnection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            Queue queue = session.createQueue(aDestinationName);
            return aCallback.performJmsFunction(session, queue);
        } catch (JMSException jmse) {
            LOG.error("Failed to create session on connection {}", aConnection);
            throw new IllegalStateException(jmse);
        } finally {
            if (session != null) {
                try {
                    session.close();
                } catch (JMSException jmse) {
                    LOG.warn("Failed to close session {}", session);
                    throw new IllegalStateException(jmse);
                }
            }
        }
    }

    private DestinationStatistics getDestinationStatisticsFor(String aDestinationName) throws Exception {
        Broker regionBroker = brokerService.getRegionBroker();
        for (org.apache.activemq.broker.region.Destination destination : regionBroker.getDestinationMap().values()) {
            if (destination.getName().equals(aDestinationName)) {
                return destination.getDestinationStatistics();
            }
        }
        throw new IllegalStateException(String.format("Destination %s does not exist on broker at %s", aDestinationName, brokerUrl));
    }
    
}

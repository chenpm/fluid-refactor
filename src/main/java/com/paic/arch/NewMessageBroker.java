package com.paic.arch;

/**
 * 新的流式API
 * @author Administrator
 *
 */
public interface NewMessageBroker {
	/**
	 * 建立连接
	 * @param url Mq地址
	 * @return 实例
	 * @throws Exception
	 */
	NewMessageBroker bindToMqBrokerAt(String url) throws Exception;
	
	/**
	 * 设置待发送的消息
	 * @param inputMessage 消息
	 * @return 实例
	 */
	NewMessageBroker sendTheMessage(String inputMessage);
	/**
	 * 发送消息到目的地
	 * @param inputQueue 目的队列
	 * @return 实例
	 */
	NewMessageBroker to(String inputQueue);
	
	/**
	 * 接收消息回复
	 * @param accountingQueue 监听的队列
	 * @return
	 */
	String waitForAMessageOn(String accountingQueue);
	
	/**
	 * 流式操作
	 * @return 实例
	 */
	NewMessageBroker and();
	/**
	 * 流式操作
	 * @return 实例
	 */
	NewMessageBroker then();
	
}

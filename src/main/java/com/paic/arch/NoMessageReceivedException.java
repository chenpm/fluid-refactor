package com.paic.arch;

public class NoMessageReceivedException extends RuntimeException {
    public NoMessageReceivedException(String reason) {
        super(reason);
    }
}
package com.paic.arch;

/**
 * 原有的方法
 * @author Administrator
 *
 */
public interface MessageBroker {
	MessageBroker createARunningEmbeddedBrokerOnAvailablePort() throws Exception;
	String getBrokerUrl();
	
	void startEmbeddedBroker() throws Exception;
	void stopTheRunningBroker() throws Exception;
	
	MessageBroker bindToBrokerAtUrl(String url);
	
	MessageBroker sendATextMessageToDestinationAt(String inputQueue, String incomingTradeMessage);
	
	String retrieveASingleMessageFromTheDestination(String accountingQueue);
	String retrieveASingleMessageFromTheDestination(String aDestinationName, final int aTimeout);

	long getEnqueuedMessageCountAt(String aDestinationName) throws Exception;
	boolean isEmptyQueueAt(String aDestinationName) throws Exception;
	
	MessageBroker andThen();
}
